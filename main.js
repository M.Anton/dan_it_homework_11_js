let openedEye = document.body.querySelectorAll('.fa-eye');
let inputs = document.body.querySelectorAll('input');

for (let input of inputs) {
    if (input.classList.contains('enter-password')) {

        for (let eye of openedEye) {
            if (eye.id === "enter-pass-eye") {

                eye.addEventListener('click', function () {

                    if (eye.classList.contains('fa-eye-slash')) {
                        eye.classList.remove('fa-eye-slash');
                        eye.classList.add('fa-eye');   
        
                        input.type = "password";
                    }
        
                    else {
                        eye.classList.remove('fa-eye');
                        eye.classList.add('fa-eye-slash');
        
                        input.type = "text";
                }
                });

            }
        }
      
        
        
    }

    if (input.classList.contains('submit-password')) {

        for (let eye of openedEye) {
            if (eye.id === "submit-pass-eye") {

                eye.addEventListener('click', function () {

                    if (eye.classList.contains('fa-eye-slash')) {
                        eye.classList.remove('fa-eye-slash');
                        eye.classList.add('fa-eye');
                        
                        input.type = "password";
                    }
        
                    else {
                        eye.classList.remove('fa-eye');
                        eye.classList.add('fa-eye-slash');
        
                        input.type = "text";
                }
                });

            }
        }

    }
}

let submitButton = document.body.querySelector('form.password-form .btn');

let text = document.createElement('p');
inputs[1].after(text);

submitButton.addEventListener('click', function (event) {
    event.preventDefault();

    if (inputs[0].value != "" && inputs[1].value != "" && inputs[0].value === inputs[1].value){
        text.textContent = '';
        alert('You are welcome!')
    }
    else{
        text.textContent = "Потрібно ввести однакові значення";
        text.style.color = 'red';
    }
})